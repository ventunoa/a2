// JavaScript Document
//cc is used to control canvas visibility
	var cc= document.getElementById("canvas");
cc.style.visibility="hidden";
//canvas creation
var c=document.getElementById("myCanvas");
var ctx=c.getContext("2d");

function bsubmit(){
	
	//following are variables used to get inputs from form and store them
	var name =document.getElementById("nameBox").value;
	var stars = document.getElementById("starBox").value;
	var x = document.querySelector('input[name="toD"]:checked').value;
	var input = document.getElementById("colour");
	var color= input.value;
	removeDiv();

	draw(name,stars,x,color);
}
function removeDiv() {
	//used to remove form entry div on completion of it so the actual card can take its place
    var elem = document.getElementById('cardEntry');
    elem.parentNode.removeChild(elem);
    return false;
}
function draw(name,stars,x,color){
cc.style.visibility="visible";
if (x=="Day"){
	ctx.fillStyle="aqua";
}
	else  {
		ctx.fillStyle="navy";
	}	

ctx.fillRect(0,0,500,500);
	//used for for loop to print stars
	var i=0;
		for (i;i<stars;i++){
		var f=Math.random()*400;
		var g=Math.random()*200;
		ctx.fillStyle="yellow";
		ctx.beginPath();
		ctx.arc(f,g,1,0,Math.PI*2);
		ctx.closePath();
		ctx.fill();
	}
	ctx.fillStyle="yellow";
	ctx.beginPath();
	ctx.arc(240,50,20,0,Math.PI*2);
			ctx.closePath();
		ctx.fill();
ctx.fillStyle=color;
ctx.font="32px Georgia";
ctx.fillText("Happy Birthday",50,115,500);
ctx.fillText(name+"!",60,160,500);
ctx.fillStyle="brown";
ctx.fillRect(35,250,50,150);
ctx.fillRect(220,280,35,100);
ctx.fillStyle ="green"; 
ctx.beginPath();
ctx.arc(155,575,225,0,Math.PI*2);
ctx.closePath();
ctx.fill();
ctx.beginPath();
ctx.arc(60,250,55,0,Math.PI*2)
ctx.closePath();
ctx.fill();
ctx.beginPath();
ctx.arc(210,225,30,0,Math.PI*2);
ctx.arc(255,230,30,0,Math.PI*2);
ctx.closePath();
ctx.fill();
ctx.beginPath();
ctx.arc(260,275,30,0,Math.PI*2);
ctx.closePath();
ctx.fill();
ctx.beginPath();
ctx.arc(215,270,30,0,Math.PI*2);
ctx.closePath();
ctx.fill();
	colors(color,name);
}
function colors(color,name){
	//used to alternate colors everytime the loop counter is properly visible by 100
var q=0;
var f=1;
	for(f;f<0;f++){
		if (f%100==0){
			q=1;
		}
	}
	if (q==0){
		ctx.fillStyle=color;
		ctx.font="32px Georgia";
		ctx.fillText("Happy Birthday",50,115,500);
		ctx.fillText(name+"!",60,160,500);
	}
	else if (q==1){
		ctx.fillStyle="orange";
		ctx.font="32px Georgia";
		ctx.fillText("Happy Birthday",50,115,500);
		ctx.fillText(name+"!",60,160,500);
		q=0;
	}
}